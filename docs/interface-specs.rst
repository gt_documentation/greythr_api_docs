.. _greythr_interface_specs:

Interface Specifications
========================

The API uses HTTP requests to receive data and will send responses in JSON/ XML format. Each request should contain a header ``Accept`` with value as ``application/json`` or ``application/xml``.

API Request
-----------

Every API call should contain API authentication parameters (see: :doc:`authentication`.)  Each account is provided with a unique account key, API user, and an API user key.

API Response
------------

Each API request call returns a JSON/ XML response. Every web service will provide the format of the JSON/ XML response. Any exceptions occurred during the call will also be sent back in a JSON/ XML format.

API Exception
-------------

If there is an exception during the processing of the request then the application will send the response in the following format along with http status 400 error code.::

 {
   "key": "<ExceptionKey>", "message": "<DetailedMessage>.", 
   "details": {"entries": [{"key": "<ErrorKey>", "value": "<ErrorValue>"}]}, 
   "exception": true
 }

- **key**: This can be used to distinguish different types of exceptions like:
	- INTERNAL_EXCEPTION
	- VALIDATION_EXCEPTION
	- ACCESS_ID_BLANK_EXCEPTION

- **message**: This is detailed message about the exception.

- **details**: Contains a list of errors with an error code and the description of the error. 

- **exception**: This field can be used to check whether the response has any exceptions.

