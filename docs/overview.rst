.. _greythr_api_intro:

=========
Overview
=========


Prerequisites
-------------

In order to access the greytHR API you will need:

- A greytHR customer account with a specific domain (e.g. mycompany.greythr.com)
- An API access ID and secret key

You can use a variety of languages to access the REST API like Java, .NET, PHP, Python, etc. We have sample code for some of these languages.

Support
-------

In case of any clarifications, please get in touch with api-support AT greytip.com

