.. _api_authentication:

API Authentication
==================

All API call has to be made using the following four parameters 

- **accessId**: Every greytHR account is provided with a unique key called ``access_id``.
- **user**: Each web service call should have a user name that should exist in greytHR. The default API user name will be ``Apiuser``. 
- **timestamp**: Current timestamp value in ISO 8601 format (``yyyy-MM-ddTHH:mm:ss.SSSZ``.) The timestamp is valid for 60 seconds from the time of creation.
- **signature**: The signature is a SHA256 hash of the timestamp value with the ``api_key`` used as the hashing key.

.. important:: 
   Each API will be provided with a unique **api-key**. However, API keys should not be passed with the request calls but just used to sign the timestamp as mentioned above.

   
Example
-------

Here is a sample of how a REST API call looks like with authentication parameters: ::

  https://api.greythr.com/rest_api/admin/app_authenticate/employee/501?accessId=349032900923



   
The following API exceptions might be thrown by the application if there are any failures during request authentication:

- INTERNAL_EXCEPTION
- ACCESS_ID_BLANK_EXCEPTION
- USER_BLANK_EXCEPTION
- TIMESTAMP_BLANK_EXCEPTION
- SIGNATURE_BLANK_EXCEPTION	
- INVALID_ACCESS_ID_EXCEPTION
- INVALID_TIMESTAMP_EXCEPTION
- INVALID_SIGNATURE_EXCEPTION     


   