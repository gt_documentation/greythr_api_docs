.. greythr_api_v1 documentation master file, created by
   sphinx-quickstart on Sun Aug 16 08:16:13 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

About greytHR API documentation
==========================================

This is REST API for external applications to integrate and interoperate with greytHR. 

Contents:

.. toctree::
   :maxdepth: 2

   overview
   interface-specs
   authentication
   employee-api
	
  
Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

