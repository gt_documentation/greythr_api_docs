.. _employee_api:

Employee API
============

This section describes about the various web service methods available for managing employees data.

Add Employee
------------

.. http:post:: /api/admin/emp-info/employee

  Add a new employee to the database.

  :<json string employeeNo: An unique employee number for each employee
  :<json string name: The full name of the employee
  
  :>json boolean success: Returns true if successful 
  
  **Example request**:
  
  .. sourcecode:: http
  
     GET /api/admin/emp-info/employee
     Host: test-api.greythr.com
     Accept: application/json, text/javascript
  
  **Example Response**:

  .. sourcecode:: http
  
     HTTP/1.1 200 OK
     Vary: Accept
     Content-Type: text/javascript
     
	 {
	   "success": true
	 }

  **Exceptions**

  ==========================  ==========================================================
  INTERNAL_EXCEPTION          There is an unspecified server error 
  VALIDATION_EXCEPTION        Input field validation has failed
  EMPLOYEE-ALREADY-EXISTS     An employee with the employee no already exists
  TRANSITION-TYPE-NOT-EXISTS  A specified category does not exist in the master list
  ==========================  ==========================================================

